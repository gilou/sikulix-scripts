

PROFILE_ID = 'gilouuuuuuuuuu'
YEAR = 2015
TYPE = 'STATUSCLUSTER'
TYPE = 'LIKEDPOSTS'
TYPE = 'TAGSBYOTHERSCLUSTER'

HISTORY_URL = 'https://www.facebook.com/{}/allactivity?category_key={}&filter_hidden=ALL&filter_privacy=NONE&year={}'.format(PROFILE_ID, TYPE, YEAR)
HISTORY_MANAGE_BUTTON = Pattern("facebook_history_item_manage.png").similar(0.53).targetOffset(0,-5)
HISTORY_DELETE_BUTTON = "HISTORY_DELETE_BUTTON.png"
HISTORY_LIKE_DONTLIKE_BUTTON = "HISTORY_LIKE_DONTLIKE_BUTTON.png"
HISTORY_DELETE_IDENTIFICATION = "HISTORY_DELETE_IDENTIFICATION.png"

MIDDLE_LEFT_PANEL = Region(68,518,591,150)

def load_url(url = HISTORY_URL):
    type('l', Key.CTRL)    
    paste(url)
    type(Key.ENTER)    

def focus_history_panel():
    with Region(668,288,1134,696):
        wait("facebook_history_select_item.png", 15)
    around_panel = MIDDLE_LEFT_PANEL
    MIDDLE_LEFT_PANEL.mouseMove()
    mouseMove(-50, 0)
    return around_panel

def delete_identification():
    with Region(675,269,638,719):
        wait("facebook_remove_tag_init.png").click()
        wait("facebook_remove_tag_confirm.png").click()
        wait("facebook_remove_tag_confirmed.png")
        wait("facebook_remove_tag_close.png").click()
        load_url()
        focus_history_panel()
        
    
try:
    switchApp('Mozilla Firefox')
    type('t', Key.CTRL)
    load_url()
    publication_counter = 0
    failed = 0
    around_panel = focus_history_panel()
    print('Deleted {} history items so far'.format(publication_counter))
    with Region(around_panel):
        while failed < 5:
            MIDDLE_LEFT_PANEL.mouseMove()
            mouseMove(-100, 0)
            wait(HISTORY_MANAGE_BUTTON, 10).click()
            wait(0.2)
            action = findAny(HISTORY_LIKE_DONTLIKE_BUTTON, HISTORY_DELETE_BUTTON, HISTORY_DELETE_IDENTIFICATION)
            if action:
                action[0].click()
                if action[0].getIndex() == 2:
                    delete_identification()
                failed = 0
                publication_counter += 1
                if action[0].getIndex() == 0 and publication_counter % 20 == 0:
                    load_url()
                    wait(1)
                    setROI(focus_history_panel())
            else:
                failed += 1
     
finally:
    wait(2)
    print('Deleted {} posts'.format(publication_counter))
    type('w', Key.CTRL)